package org.fn.ie.repository;

public class FoodDAO {
    private String name;
    private String description;
    private float popularity;
    private int price;
    private String image;
    private String restaurantName;

    public String getName() { return name; }

    public String getDescription() { return description; }

    public float getPopularity() { return popularity; }

    public String getRestaurantName() { return restaurantName; }

    public int getPrice() { return price; }

    public String getImage() { return image; }

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPopularity(float popularity) {
        this.popularity = popularity;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public void setRestaurantName(String restaurantName) {
        this.restaurantName = restaurantName;
    }
}
